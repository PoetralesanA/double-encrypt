﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox2.Text = String.Empty
        If RadioButton2.Checked = True Then
            TextBox2.Text = MD5.Encrypt(TextBox1.Text, TextBox3.Text)
        Else
            If RadioButton3.Checked = True Then
                TextBox2.Text = AES.Encrypt(TextBox1.Text, TextBox3.Text)
            Else
                TextBox2.Text = Combine.Encrypt(TextBox1.Text, TextBox3.Text)
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If RadioButton2.Checked = True Then
            TextBox2.Text = MD5.Decrypt(TextBox1.Text, TextBox3.Text)
        Else
            If RadioButton3.Checked = True Then
                TextBox2.Text = AES.Decrypt(TextBox1.Text, TextBox3.Text)
            Else
                TextBox2.Text = Combine.Decrypt(TextBox1.Text, TextBox3.Text)
            End If
        End If
    End Sub
End Class