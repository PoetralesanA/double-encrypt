﻿Public Class Combine
    Public Shared Function Encrypt(ByVal val As String, key As String) As String
        Return AES.Encrypt(MD5.Encrypt(val, key), key)
    End Function
    Public Shared Function Decrypt(ByVal val As String, key As String) As String
        Return MD5.Decrypt(AES.Decrypt(val, key), key)
    End Function
End Class